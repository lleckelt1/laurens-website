const boardWidth = 6;
const boardHeight = 8;
const gameBoard = document.getElementById("game-board");
const scoreDisplay = document.getElementById("score-value");

let score = 0;
let board = [];

const fruits = ["🍎", "🍊", "🍋", "🍒"];  // Different fruit types

// Initialize the board
function createBoard() {
    board = Array.from({ length: boardHeight }, () => Array(boardWidth).fill(null));
    gameBoard.innerHTML = "";
    for (let i = 0; i < boardHeight * boardWidth; i++) {
        const cell = document.createElement("div");
        cell.id = `cell-${i}`;
        gameBoard.appendChild(cell);
    }
}

// Generate a random fruit
function getRandomFruit() {
    return fruits[Math.floor(Math.random() * fruits.length)];
}

// Drop a new fruit and make it fall to the bottom of a random column
function dropFruit() {
    const fruit = getRandomFruit();
    let column = Math.floor(Math.random() * boardWidth);

    // Find the lowest empty cell in the chosen column
    for (let row = boardHeight - 1; row >= 0; row--) {
        if (!board[row][column]) {
            board[row][column] = fruit;
            updateBoard();
            checkMerge(row, column);
            return;
        }
    }

    // If the column is full, skip dropping the fruit
}

// Update the visual representation of the board
function updateBoard() {
    for (let row = 0; row < boardHeight; row++) {
        for (let col = 0; col < boardWidth; col++) {
            const cell = document.getElementById(`cell-${row * boardWidth + col}`);
            cell.textContent = board[row][col] || "";
        }
    }
}

// Check for matching adjacent fruits and merge
function checkMerge(row, col) {
    const fruit = board[row][col];
    if (!fruit) return;

    const directions = [
        { r: -1, c: 0 }, { r: 1, c: 0 },
        { r: 0, c: -1 }, { r: 0, c: 1 },
    ];

    directions.forEach(({ r, c }) => {
        const newRow = row + r;
        const newCol = col + c;
        if (
            newRow >= 0 && newRow < boardHeight &&
            newCol >= 0 && newCol < boardWidth &&
            board[newRow][newCol] === fruit
        ) {
            // Remove both fruits after merging
            board[row][col] = null;
            board[newRow][newCol] = null;

            // Update score and refresh the display
            score += 10;
            scoreDisplay.textContent = score;

            updateBoard();
            dropFruit();  // Drop another fruit after merging
        }
    });
}

// Start the game
function startGame() {
    score = 0;
    scoreDisplay.textContent = score;
    createBoard();
    setInterval(dropFruit, 1000);  // Drop a fruit every second
}